catalog:
  title: OpenCV Face Recognition
  version: nightly

variables:
  recipe.maintainer:
  - flier@techfak.uni-bielefeld.de

  platform-requires:
    precise:
      packages:
      - libhighgui2.3
      - libcv2.3
      - libopencv-gpu2.3
      - libcvaux-dev
      - libcv-dev
      - libhighgui-dev
      - libopencv-gpu-dev
      - ros-hydro-ros-base
      - ros-indigo-usb-cam
    trusty:
      packages:
      - libhighgui2.4
      - libcv2.4
      - libopencv-gpu2.4
      - libcvaux-dev
      - libcv-dev
      - libhighgui-dev
      - libopencv-gpu-dev
      - ros-indigo-ros-base
      - ros-indigo-usb-cam
    ubuntu:
      packages:
      - cmake
      - build-essential
      - libgstreamer0.10-dev
      - libgstreamer-plugins-base0.10-dev
      - gstreamer0.10-alsa
      - gstreamer0.10-plugins-base
      - gstreamer0.10-plugins-good
      - gstreamer0.10-tools
      - gstreamer0.10-pulseaudio

  toolkit.dir: ${toolkit.volume}/${distribution-name}
  toolkit.volume: /tmp/

  prepare-hook-name: distribution-prepare-${distribution-name}
  prepare-hook/unix: |
    mkdir -p "${toolkit.dir}"
    chmod 2755 "${toolkit.dir}"

  finish-hook-name: distribution-finish-${distribution-name}
  finish-hook/unix: |
    find "${toolkit.dir}" -type d -exec chmod 2755 {} \;
    find "${toolkit.dir}" -type f -exec chmod go+r {} \;
    find "${toolkit.dir}" -type f -perm /100 -exec chmod go+x {} \;
    chmod -R g+x "${toolkit.dir}"/bin

versions:
- spread                                                    @4.4
- rsc                                                       @0.11
- rsb-protocol                                              @0.11
- rsb-cpp                                                   @0.11
- rsb-python                                                @0.11
- rsb-tools-cpp                                             @0.11
- rsb-spread-cpp                                            @0.11
- rsb-tools-cl-binary                                       @0.11
- rsbag-tools-cl-binary                                     @0.11
- rst-proto                                                 @0.11
- rst-converters-cpp                                        @0.11
- rst-converters-python                                     @0.11
- rsb-opencv                                                @0.11
- pyscxml                                                   @v0.8.3
- fsmt                                                      @0.16
- ocvfacerec                                                @master
