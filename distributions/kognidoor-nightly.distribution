catalog:
  title: KogniDoor
  version: nightly

variables:
  access: private
  datadir: /vol/csra/data/${variant}/
  variant: nightly

  platform-requires:
    ubuntu:
      packages:
      - cmake
      - build-essential
      - libprotobuf-dev
      - protobuf-compiler
      - libboost-dev
      - python-setuptools
      - python-dev
      - python-protobuf
      - libboost-thread-dev
      - libboost-filesystem-dev
      - libboost-signals-dev
      - libboost-program-options-dev
      - libboost-system-dev
      - libboost-regex-dev
      - liblog4cxx10-dev
      - libffi-dev
      - libpcl-dev
      - ocl-icd-opencl-dev
      - opencl-headers

  toolkit.dir: ${toolkit.volume}/${distribution-name}
  toolkit.volume: /tmp/

  prepare-hook-name: distribution-prepare-${distribution-name}
  prepare-hook/unix: mkdir -p "${toolkit.dir}"

versions:
- kognidoc                                                  @master
- kogniserver                                               @master
- kognihome-displayserver-extras                            @master
- kognidoor-eink                                            @master
- kognidoor-frontend                                        @master
- kognihome-doorcontroller                                  @master
- kognidoor-image-server                                    @master
- kognidoor-vision                                          @master
- rsbhsm                                                    @toolkit
- python-transitions                                        @master
- kognidoor-ble-grabber                                     @bethel-door
- kognicom                                                  @master
- kognidoor-dispatcher                                      @master
- kogniconfig                                               @master
- lsp-csra-system-startup                                   @master
- lsp-csra-system-config                                    @master
- kognijenkins                                              @master
- icl-kognihome                                             @trunk
- bullet                                                    @master
- vdemo                                                     @master
- spread                                                    @4.4
- spread-python                                             @1.5spread4
- rsc                                                       @0.15
- rsb-protocol                                              @0.15
- rsb-cpp                                                   @0.15
- rsb-python                                                @0.15
- rsb-spread-cpp                                            @0.15
- rst-proto-csra-workaround                                 @0.15
- rst-proto-kognihome                                       @0.15
- rst-converters-cpp                                        @0.15
- rst-converters-python                                     @0.15
- rsb-xml-cpp                                               @0.15
- rsb-xml-python                                            @0.15
- rsb-tools-cpp                                             @0.15
- rsb-tools-cl-binary                                       @0.15
- rsbag-tools-cl-binary                                     @0.15
- nodejs                                                    @v6.9.1
- peerjs-server                                             @master
- kognidoor-obstacledetection                               @master
- kognidoor-lf-grabber                                      @master
