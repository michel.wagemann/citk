minimum-generator-version: 0.23.48

catalog:
  title: OpenCV 3 plus Contrib Modules

templates:
- patches
- github
- cmake-cpp
- base

variables:
  description: OpenCV from source and contrib modules
  keywords:
  - image processing
  - opencv
  - opencv-contrib
  recipe.maintainer:
  - flier@techfak.uni-bielefeld.de

  github.user: opencv
  github.project: opencv
  git.shallow?: true
  branches:
  - master
  - 3.1.0
  - 3.2.0
  - 3.3.0
  - 3.3.1
  - 3.4.0
  - 3.4.1
  - 3.4.5
  - 3.4.8
  - 4.1.2

  commit.contrib: ${version-name}

  timeout/minutes: 150

  numeric-version: ${version-name}
  extra-provides:
  - nature: cmake
    target: opencv
    version: ${numeric-version}
  - nature: cmake
    target: opencv-plus-contrib-cuda
    version: ${numeric-version}
  extra-requires:
  - freestyle: cuda
  platform-requires:
    ubuntu:
      packages:
      - '@{next-value|[]}'
      - libgphoto2-dev
      - libavresample-dev
      - libtbb-dev
      - libtbb2
      bionic:
        packages:
        - lvtk-dev

  cmake.options:
  - CMAKE_BUILD_TYPE=RELEASE
  - BUILD_TESTS=OFF
  - BUILD_PERF_TESTS=OFF
  - WITH_QT=ON
  - WITH_GSTREAMER=OFF
  - WITH_OPENGL=ON
  - WITH_OPENCL=OFF
  - WITH_V4L=ON
  - WITH_TBB=ON
  - WITH_XIMEA=OFF
  - WITH_IPP=ON
  - BUILD_opencv_face=ON
  - CUDA_GENERATION=${cudagen}
  - CUDA_HOST_COMPILER=${cuda.host.compiler|\$(which g++)}
  - CUDA_TOOLKIT_ROOT_DIR=${toolkit.dir}
  - WITH_CUDA=ON
  - ENABLE_FAST_MATH=1
  - CUDA_FAST_MATH=1
  - WITH_CUBLAS=1
  - OPENCV_EXTRA_MODULES_PATH=../opencv_contrib/modules
  - '@{next-value|[]}'

  patches.diff-strings:
    - !b!literal-include patches/opencv/opencv3_cuda11.diff
    - !b!literal-include patches/opencv/opencv_all.patch

  pre-build-hook/unix.command: |
    test -d opencv_contrib || git init opencv_contrib
    (
      cd opencv_contrib
      git fetch --tags --progress --depth=1 https://github.com/opencv/opencv_contrib.git ${commit.contrib}
      git checkout -f ${commit.contrib}
    )

versions:
- name: tiago-clf
  variables:
    tag: 4.1.2
    commit.contrib: 4.1.2
    cmake.options:
    - PROTOBUF_UPDATE_FILES=ON
    - BUILD_PROTOBUF=OFF
    - BUILD_EXAMPLES=OFF
    - '@{next-value|[]}'

- name: cuda-10
  variables:
    numeric-version: 3.4.11
    tag: 3.4.11
    commit.contrib: 3.4.11
    cmake.options:
    - WITH_NVCUVID=OFF
    - BUILD_EXAMPLES=OFF
    - '@{next-value|[]}'

- name: 3.2.0-avx-fix
  variables:
    numeric-version: 3.2.0
    commit: 51e16bbe81a679eaef2d0b3cd30e5ac96a798854
    commit.contrib: 8f23155d5972940ace3030ca82a9e65f55237d21

- name: 3.3.1
  variables:
    tag: 3.3.1

    patches.diff-strings:
    - !b!literal-include patches/opencv/opencv-3.1.1-relwithdepinfo.diff

- pattern: '^[0-9]+\.[0-9]+\.[0-9]+$'
  variables:
    extra-requires:
    - nature: program
      target: protoc
    tag: ${match:0}
    commit.contrib: ${match:0}
    cmake.options:
    - BUILD_EXAMPLES=OFF
    - '@{next-value|[]}'