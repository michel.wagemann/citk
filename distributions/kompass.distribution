catalog:
  title: KOMPASS
  version: ${variant}

variables:
  access: private

  toolkit.dir: ${toolkit.volume}
  toolkit.volume: /vol/kompass

  filesystem.group/unix: kompass

  prepare-hook-name: distribution-prepare-${distribution-name}
  prepare-hook/unix: |
    mkdir -p "${toolkit.dir}"
    mkdir -p "${toolkit.dir}/var"
    chgrp ${filesystem.group/unix} "${toolkit.dir}"
    chgrp ${filesystem.group/unix} "${toolkit.dir}/var"
    chmod u=rwx,go=rx "${toolkit.dir}"
    chmod u=rwx,g=rwxs,o=rx "${toolkit.dir}/var"

  finish-hook-name: distribution-finish-${distribution-name}
  finish-hook/unix: |
    chgrp -R ${filesystem.group/unix} "${toolkit.dir}"
    chmod -R u=rwX,go=rX "${toolkit.dir}"
    chmod -R g+w "${toolkit.dir}/var"
    find "${toolkit.dir}/var" -type d -exec chmod g+s {} \;

  variant: nightly
  make.threads: '4'
  keep-builds/count: 2

versions:
- cmake                                                     @3.9.2
- ffmpeg                                                    @master
- nasm                                                      @current
- libx264                                                   @stable
- libx265                                                   @stable
- lame                                                      @current
- xvid                                                      @current
- libvpx                                                    @current
- vidstab                                                   @master
- fdk-aac                                                   @current
- libass                                                    @current
- praat                                                     @current
- mediainfo                                                 @current
- mpv-player                                                @master
- essentia                                                  @master
- spread                                                    @4.4
- spread-python                                             @1.5spread4
- vdemo                                                     @master
- kompass-system-startup                                    @master
- rsc                                                       @master
- rsb-protocol                                              @master
- rsb-cpp                                                   @master
- rsb-python                                                @master
- rsb-spread-cpp                                            @master
- rsb-tools-cpp                                             @master
- rsb-tools-cl-binary                                       @master
- rsbag-tools-cl-binary                                     @master
- rst-proto                                                 @master
- rst-converters-cpp                                        @master
- rst-converters-python                                     @master
- rsb-jack                                                  @master
- rsb-video-writer                                          @master
- opencv                                                    @3.4.1-kompass
- dlib                                                      @master
- dataset-dlib                                              @master
- webrtc-audioproc                                          @master
- ipaaca                                                    @kompass
- gazetool                                                  @noddetection-openface
- libesmeraldamfcc                                          @master
- audioeventclassifier                                      @master
- cueaggregator                                             @master
- kompassdatarecorder                                       @master
