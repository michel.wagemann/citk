catalog:
  title: NAO minimal
  version: nightly
  description: opensource://nao-minimal-nightly.md
  image:
  - catalog://nao-minimal.png

variables:
  recipe.maintainer:
  - mwiechmann@techfak.uni-bielefeld.de
  - flier@techfak.uni-bielefeld.de

  toolkit.dir: ${toolkit.volume}/${distribution-name}
  toolkit.volume: /tmp/

  prepare-hook-name: distribution-prepare-${distribution-name}
  prepare-hook/unix: |
    mkdir -p "${toolkit.dir}/opt"
    chmod 2755 "${toolkit.dir}"

  finish-hook-name: distribution-finish-${distribution-name}
  finish-hook/unix: |
    find "${toolkit.dir}" -type d -exec chmod 2755 {} \;
    find "${toolkit.dir}" -type f -exec chmod go+r {} \;
    find "${toolkit.dir}" -type f -perm /100 -exec chmod go+x {} \;
    chmod -R g+x "${toolkit.dir}"/bin

versions:
- libicu                                                    @52.1
- libhdf5                                                   @1.8.13
- aldebaran-naoqi-sdk-cpp                                   @2.4.3.28
- aldebaran-naoqi-sdk-python27                              @2.4.3.28
- fsmt                                                      @0.19
- pyscxml                                                   @v.0.8.5-fsmt
- fsmt-exp-nao-minimal                                      @master
- runnable-nao-minimal                                      @master
