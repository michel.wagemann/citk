catalog:
  title: LSP CSRA
  version: ${variant}

variables:
  make.threads: '2'
  variant: '0.13'
  flavor: xenial
  datadir: ${toolkit.volume}/data/${flavor}/${distribution-name}/
  ros: kinetic
  rstexperimental.exit_on_ambiguity: '1'
  access: private

  toolkit.volume: /vol/csra/
  toolkit.dir: ${toolkit.volume}/releases/${flavor}/${distribution-name}

  filesystem.group/unix: csra

  prepare-hook-name: distribution-prepare-${distribution-name}
  prepare-hook/unix: |
    #!/bin/bash
    mkdir -p "${toolkit.dir}"/{bin,etc,opt,share,var}
    mkdir -p "${datadir}"
    ln -vfs -T "${datadir}" "${toolkit.dir}/share/data"
    chgrp ${filesystem.group/unix} "${toolkit.dir}"
    chgrp ${filesystem.group/unix} "${toolkit.dir}/var"
    chmod u=rwx,go=rx "${toolkit.dir}"
    chmod u=rwx,g=rwxs,o=rx "${toolkit.dir}/var"

  finish-hook-name: distribution-finish-${distribution-name}
  finish-hook/unix: |
    chgrp -R ${filesystem.group/unix} "${toolkit.dir}"
    chmod -R u=rwX,go=rX "${toolkit.dir}"
    chmod -R g+w "${toolkit.dir}/var"
    find "${toolkit.dir}/var" -type d -exec chmod g+s {} \;

  java.jdk: Java8
  bco.registry.db.overwrite: 'TRUE'
  bco.registry.db.git.path: release-0.13
  maven.dependency-versions:
  - dependency.rsx.version=[0.17,0.18-alpha)
  gstreamer-version: '1.14'

versions:
- spread                                                    @4.4
- spread-python                                             @1.5spread4
- sbcl-binary                                               @1.3.0
- quicklisp                                                 @2017-08-30
- cl-launch                                                 @4.1.3-for-sbcl-binary
- cl-iterate-sequence                                       @hash-e116f4a
- cl-architecture.service-provider                          @0.4
- cl-architecture.builder-protocol                          @0.8
- cl-network.spread                                         @0.3
- cl-protobuf                                               @0.2
- rsc                                                       @0.17
- rsb-protocol                                              @0.17
- name: rsb-cpp
  version: '0.17'
  parameters:
    branch: wip-scope-dispatcher
- rsb-cl                                                    @0.17
- rsb-java                                                  @0.17
- name: rsb-python
  version: '0.17'
  parameters:
    branch: wip-connection-pooling
- rsb-matlab                                                @0.17
- rsb-tools-cpp                                             @0.17
- name: rsb-tools-cl
  versions:
  - version: 0.17-feature-norman-bridge
  - version: 0.17-rsb-only
- name: rsb-spread-cpp
  version: '0.17'
  parameters:
    branch: wip-connection-pooling
- rsb-xml-cpp                                               @0.17
- rsb-xml-java                                              @0.17
- rsb-xml-python                                            @0.17
- rst-proto                                                 @0.17
- rst-converters-cpp                                        @0.17
- rst-converters-python                                     @0.17
- name: rsbag-cl
  versions:
  - version: feature-online-size-tracking-0.15
  - version: '0.17'
- rsbag-tools-cl                                            @0.17-rsb-only
- rsb-process-monitor                                       @hash-2.0
- xacro                                                     @jade-devel
- libreflexxes                                              @1.2.6
- humotion                                                  @0.2-ros-only
- xsc3                                                      @0.2
- flobi-description                                         @0.2
- flobi-sim                                                 @hash-18.02
- hlrc_server                                               @csra-0.2
- hlrc_client_python                                        @csra-0.2
- hlrc_tts_provider                                         @csra-0.2
- python3-rospkg                                            @1.0.20
- python3-catkin-pkg                                        @0.1.10
- pyscxml                                                   @v.0.8.5-fsmt
- fsmt                                                      @0.5
- vdemo                                                     @hash-18.09
- rsbag-python                                              @0.17
- rsb-gstreamer                                             @0.17
- rsb-buffer                                                @0.17
- rsb-opencv                                                @hash-csra-0.11
- rsb-depth-sensors                                         @extension_2
- link-rsb-tools-cl                                         @rsb-toolscl0.17
- link-rsbag-tools-cl                                       @rsbagcl0.17
- rsb-lazy-converter-python                                 @0.17
- rst-experimental-proto                                    @0.17
- lsp-csra-recording-statistics-graphite-adapter            @0.1
- lsp-csra-rsb-introspection-graphite-adapter               @0.1
- rsb-person-tracking                                       @hash-18.09
- bayestracking                                             @features_bi-opencv3
- pro-pm                                                    @v0.5.1
- icl-github                                                @10.0.0-csra
- openni2                                                   @hash-17.12
- rsb-host-monitor                                          @hash-2.0
- avin2-sensorkinect                                        @v0.93-5.1.2.1
- apache-jena-fuseki-server                                 @3.7.0
- openbase-developer-tools                                  @v0.1.4
- jps                                                       @v3.4.8
- jul                                                       @v1.6.10
- openhab-runtime                                           @v1.8.3.2
- openhab-addons                                            @v0.3.2
- openhab-cfg                                               @v0.4.0
- openhab-binding-rsb                                       @v1.6.0
- bco.authentication                                        @v1.6.4
- bco.registry                                              @v1.6.8
- bco.registry.editor                                       @v1.6.4
- bco.registry.csra-db-deployer                             @release-0.13
- bco.dal                                                   @v1.6.8
- bco.manager                                               @v1.6.9
- bco.ontology                                              @v1.6.0
- bco.ontology.lib                                          @v1.6.1
- bco.psc                                                   @v1.6.0
- generic-display                                           @v0.6.17
- bco.bcozy                                                 @v1.6.4
- bco.stage                                                 @v1.6.1
- bco.eveson                                                @v1.6.2
- pamini                                                    @1.16
- dialogflow                                                @1.13
- marytts-timobaumann                                       @incrementalityChangesTake2
- inprotk                                                   @csra_mary5
- inprotk-csra-conf                                         @1.9
- speech-command-proposer                                   @1.8
- speech-visualizer                                         @1.8
- dialog-visualizer                                         @1.8
- sphinx-rst-builder                                        @1.5
- jsgf-parser                                               @4.0
- verbmobil-model                                           @0.3
- cocolab-model                                             @0.10
- rsbperfmon-db-adapter                                     @csra-hash-0.8
- graphite-env                                              @stable
- graphite-web                                              @hash-csra-0.3
- carbon                                                    @hash-csra-0.3
- grafana                                                   @latest
- humavips-facedetection                                    @csra-0.1
- humavips-facetracker                                      @csra-0.1
- humavips-idrecognition                                    @learningver
- distributed_faceid_control                                @hash-18.09
- dsl-ide-cor-lab-workbench                                 @49
- rsb-scxml-engine                                          @csra-0.17
- rst-utils                                                 @0.4
- rta-lib                                                   @1.4
- arbitration-service                                       @0.14
- csra-scenario-coordination-cfg                            @15.1
- highlight-service                                         @0.12
- aa-skills                                                 @0.8
- location-light                                            @hash-18.09
- ltm-core                                                  @v0.5
- neo4j                                                     @2.2.10
- mongodb                                                   @ubuntu1404-3.2.7
- ltm-java                                                  @0.4
- 2d-map-visualisation                                      @0.8
- object-detectionmock                                      @2.2
- lsp-csra-system-startup                                   @release-0.13
- lsp-csra-system-config                                    @release-0.13
- xqilla                                                    @2.3
- pquery                                                    @v1.4.0
- sitinf                                                    @hash-18.09
- caldavcalendar                                            @1.9
- hookah                                                    @hash-18.09
- rsb-jack                                                  @hash-18.09
- webrtc-audioproc                                          @hash-18.02
- csra-resources                                            @release-0.8
- libinkg                                                   @v0.4
- inkg-rsb-interface                                        @hash-18.09
- csra-utils                                                @release-0.13
- study-control                                             @v1.1.5
- lsp-csra-video-tools                                      @0.3
- csra-robot-transforms                                     @v0.1.0
- citk-scm-access-check                                     @0.2
- floor-base                                                @v0.5.0
- floor-lib                                                 @v0.2.1
- tf2_py                                                    @0.5.9
- floor-view                                                @v0.5.0
- floor-input-emulator                                      @v0.4.0
- rct-tools                                                 @1.0.1
- rct-cpp                                                   @1.0.1
- rct-python                                                @1.0.3
- rct-java                                                  @v1.2.2
- lsp-csra-docs                                             @release-0.13
- csra-personmanager                                        @release-0.1
- csra-screen-utils                                         @0.3.3
- ijenkinscli                                               @0.1
- dlib                                                      @v19.14
- gazetool                                                  @rsb-18-07-12
- pontoon                                                   @2.0
- ola                                                       @0.10.0
- csra-movinghead                                           @hash-18.09
- csra-daemon-services                                      @hash-18.09
- csra-soundscapes                                          @hash-18.09
- ts-jenkins-connector                                      @0.4
- dataset-dlib                                              @dlib.net
- ximea-api                                                 @hash-18.04
- simple-robot-gaze-tools-audio                             @hash-csra-0.3
- hlrc_simple_robot_gaze                                    @rsb_support
- hlrc_simple_robot_gaze_plugins                            @hash-csra-0.3
- opencv-contrib                                            @3.4.0
- opencv3                                                   @3.4.0
- cuda                                                      @8.0
- cudnn                                                     @6.0
- caffe                                                     @hash-18.09
- cmu-openpose                                              @hash-18.09
- citk-version-updater                                      @v0.3.1
- csra-release-utils                                        @v0.3.1
- csra-proximity-tools                                      @v0.1.0
- rfid-device-manager-mockup                                @v1.3.0
- fformation                                                @v2.1.1
- fformation-rsb                                            @v1.0.0
- fsmt-exp-csra-experiments                                 @hash-18.09
- ffmpeg                                                    @hash-18.05.18
- nasm                                                      @2.13.03
- libx264                                                   @hash-18.05.18
- libx265                                                   @d3b714
- lame                                                      @3.100
- xvid                                                      @1.3.5
- libvpx                                                    @1.7.0
- vidstab                                                   @afc8ea9
- fdk-aac                                                   @0.1.6
- libass                                                    @0.14.0
- praat                                                     @6.0.37
- mediainfo                                                 @18.03
- mpv-player                                                @hash-18.05.18
- fktool                                                    @hash-18.05.18
- orc                                                       @orc-0.4.28
- openh264                                                  @v1.7.0
- gstreamer                                                 @${gstreamer-version}
- gstreamer-plugins-base                                    @${gstreamer-version}
- gstreamer-plugins-good                                    @${gstreamer-version}
- gstreamer-plugins-bad                                     @${gstreamer-version}
- gstreamer-plugins-ugly                                    @${gstreamer-version}
- gstreamer-vaapi                                           @${gstreamer-version}
- gstreamer-libav                                           @${gstreamer-version}
