catalog:
  title: Tiago Software
  version: melodic-nightly

variables:
  description: Clean Up robot software ontop of Tiago Melodic Dist
  recipe.maintainer:
    - Leroy Rügemer <lruegeme@techfak.uni-bielefeld.de>

  toolkit.volume:    /vol/tiago/melodic
  toolkit.dir:       ${toolkit.volume}/yolox

  ros.underlay: /dev/null
  ros: melodic
  access: private

  cmake.options:default:
    - CUDA_TOOLKIT_ROOT_DIR=${toolkit.dir}
    - CUDA_NVCC_FLAGS="--expt-relaxed-constexpr"
    - '@{next-value|[]}'

  shell.environment:default:
    - PATH=${toolkit.dir}/bin:\${PATH}
    - ROS_PYTHON_VERSION=${python.version}
    - PYTHONPATH=${toolkit.dir}/lib/python3/dist-packages
    - '@{next-value|[]}'

  java.jdk: Java11

  python.version: 3

  prepare-hook-name: distribution-prepare-${distribution-name}
  prepare-hook/unix: |
    mkdir -p ${toolkit.dir}
    cp -a ${base-deps.dir}/* ${toolkit.dir}/

    for d in include lib/python${python.version}/dist-packages bin share; do mkdir -p "${toolkit.dir}/\$d"; done
    cd ${toolkit.dir}
    ln -s lib lib64
    cd lib 
    ln -s python${python.version} python\${majorminor}
    cd python${python.version}
    ln -s dist-packages site-packages

    # Copy Python stuff into correct folder
    cd ${toolkit.dir}
    cp -r lib/python2.7/dist-packages/* lib/python3/dist-packages/
    rm -rf lib/python2.7

  cuda-version: ${next-value|11.6}
  cuda.host.compiler: \$(which g++-9)
  base-deps.dir: ${toolkit.volume}/cuda${cuda-version}

include:
  - stacks/tiago/yolox


versions:

# Broken
- name: pip-pip
  version: "21.3.1"

# Deploy Volume to Robot
- name: deploy-volume
  version: master
  parameters:
    user: pal
    group: pal
    remote-host: tiago-47c

## Deps
- ros-vision_msgs@noetic-devel
- ros-common_msgs@noetic-devel
- ros-dynamic_reconfigure@noetic-devel

# Bootstrap python3 ros
- name: pip-install
  versions:
  - version: ros
    parameters:
      pip.packages:
        - "rospkg"
        - "catkin_pkg"
        - "rosdep"
        - "rosinstall_generator"
        - "vcstool"
        - "rosinstall"
        - "empy"
        - "osrf-pycommon==1.0.3"
        - "defusedxml"
  - version: pip
    parameters:
      pip.packages:
        - "pip==21.3.1"
  - version: melodic-yolox
    parameters:
      pip.packages:
        - "onnxoptimizer==0.2.7"
  - version: robocup_sound_play
    parameters:
      pip.packages:
        - "simpleaudio"

- catkin@noetic-devel
- ros_comm-isolated@melodic
- catkin_tools@main

- name: ros-vision_opencv # image_geometry cv_bridge
  version: ${ros}-clf 

- ros-robocup_sound_play@master

