minimum-generator-version: '0.31.25'

inherit:
- job-defaults
- permissions
- jenkins-console-timestamper
- jenkins-console-ansi-color
- timeout-defaults
- shell-hooks-defaults
- repository-defaults
- retention-defaults
- redmine-defaults
- slave-defaults
- sloccount
- warnings
- tasks
- email-notification
- description
- deployment-test
- system-packages
- python-version

variables:
  mode: ci-deploy

  junit.allow-empty-results?: ${next-value|true}

  make.command: |
    # Build
    ${make.build.command}

    # Test
    ${make.test.command}

    # Install
    ${make.install.command}

  description.deployment: CI deployment

  build-dir: build

  dependency-dir: ${toolkit.dir}

  # This directory is used to access programs and data produced by
  # upstream jobs. Since installation typically goes into a "volume"
  # directory, meaning that PROGRAM is installed as
  #
  #   ${toolkit.dir}/bin/PROGRAM
  #
  # toolkit.dir, dependency-dir and dependency-install-dir
  # are all identical.
  dependency-install-dir: ${dependency-dir}

  dependencies.mode: ${next-value|minimal}

  scm.trigger-disable?: ${next-value|false}
  scm.trigger-spec: ${next-value|H/15 * * * *}

aspects:
- name: scm.poll
  aspect: trigger/scm
  conditions:
    scm.trigger-disable?: 'false'
    scm: git|svn
  variables:
    aspect.trigger/scm.spec: ${scm.trigger-spec}
