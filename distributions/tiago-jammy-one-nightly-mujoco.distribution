catalog:
  title: Tiago Software
  version: jammy-one-nightly-mujoco
  replication: |
    You need to install [Nightly](/distribution/tiago-jammy-one-nightly.xml) 
    to use this distribution.

  platforms:
    - "ubuntu jammy x86_64" 

variables:
  description: Clean Up robot software ontop of Tiago jammy-one Dist
  recipe.maintainer:
    - Leroy Rügemer <lruegeme@techfak.uni-bielefeld.de>

  toolkit.volume:    /vol/tiago/one
  toolkit.dir:       ${toolkit.volume}/nightly-mujoco

  access: private
  ros.lang.disable: [geneus,genlisp]

  ros.underlay: ${toolkit.volume}/nightly
  ros: one
  python.version: 3
  cudagen: 0
  cuda.arch: "6.1;8.6"
  java.version: 17

  cmake.options:default:
    - CUDA_TOOLKIT_ROOT_DIR=${toolkit.dir}
    - CUDA_NVCC_FLAGS="--expt-relaxed-constexpr"
    - '@{next-value|[]}'

  shell.environment:default:
    - PYTHONPATH=${toolkit.dir}/lib/python3/dist-packages:\${PYTHONPATH}
    - ROS_MAVEN_REPOSITORY=file://${toolkit.dir}/share/repository/
    - ROS_MAVEN_DEPLOYMENT_REPOSITORY=${toolkit.dir}/share/repository/
    - '@{next-value|[]}'

  prepare-hook-name: distribution-prepare-${distribution-name}
  prepare-hook/unix: |
    mkdir -p ${toolkit.dir}

    for d in include lib/python3/dist-packages bin share; do mkdir -p "${toolkit.dir}/\$d"; done
    cd ${toolkit.dir}
    cd lib
    ln -s python3 python3.10
    cd python3
    ln -s dist-packages site-packages

  cuda-version: ${next-value|11.6}
  #cuda.host.compiler: \$(which g++-9)
  base-deps.dir: ${toolkit.volume}/cuda${cuda-version}

include:
  - stacks/mujoco-ros

versions:

- ros-control@dleins-obese-devel
- ros-mujoco_ros_control_pal@noetic-devel

# TODO
#- stacks/pocketsphinx currently broken sphinxbase breaks ubuntu/gazebo + pocketsphinx does not build