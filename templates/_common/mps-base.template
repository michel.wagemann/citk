variables:
  natures:
  - mps-plugin-build-file

  extra-requires:
  - nature: program
    target: mps.sh
    version: ${mps-version}
  - nature: program
    target: java
    version: ${mps-java-version}
  - nature: program
    target: javac
    version: ${mps-java-version}
  - program: ant
  - '@{next-value|[]}'

  mps-java-version: '11.0'

  mps-home-directory: ${mps-home-directory:${mode}|${mps-home-directory:default}}
  mps-home-directory:default: ${dependency-dir}/mps-${mps-version}

  language-directory: ${dependency-dir}

  default-build-file:
  - build.xml
  build-file-name: ${next-value|${analysis.plugin-build-files|${default-build-file}}}

  home-variables: ${next-value|${analysis.home-variables}}

  ant-targets: ${next-value|clean generate assemble}

  build-artifact-directory: build/artifacts

  home-variable-options:
    ' -D${home-variables}="${language-directory}" '

  ant-calls: |
    ant -Dmps_home="${mps-home-directory}" @{home-variable-options} -file "${build-file-name}" ${ant-targets}

  build-properties-move-call: |
    for dir in ${build-artifact-directory}/* ; do
      if [ -e $dir/build.properties ] ; then
        mv $dir/build.properties $dir/\$(basename $dir)-build.properties
      fi
    done || true

  shell.command: ${shell.command:${mode}|${shell.command:default}}
  shell.command:default: |
    mkdir -p "${language-directory}"

    # Invoke ant for each plugin build file
    @{ant-calls}
    ${build-properties-move-call}

aspects:
- name: mps.shell
  aspect: shell
  variables:
    aspect.builder-constraints.shell:
    - [ after, { type: dependency-download } ]

    aspect.shell.command: ${shell.command}
