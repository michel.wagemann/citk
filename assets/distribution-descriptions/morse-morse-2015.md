The system is currenty developed for the CITEC MORe program. The CITEC Month of Open Research (MORe) is an international
program of the Excellence Cluster Cognitive Interaction Technology (CITEC) that offers students from abroad stipends to
contribute to open source or open data projects related to the research areas of cognitive interaction technology, like
motion intelligence, attentive systems, conversational agents, memory retrieval and learning.

https://cit-ec.de/en/more/motion-tracking-based-teleoperation-virtual-human-morse