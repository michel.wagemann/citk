inherit:
- orchestration-base
- orchestration-hooks
- orchestration-functions

variables:
  mode: toolkit

  orchestration.control.explanation: This job builds all projects contained in the
    ${distribution-name} distribution in the correct order and with maximum parallelity.

  orchestration.control.parameters:
  - name: ageLimit
    kind: string
    default: none
    description: >-
      If "none", Jenkins will initiate a build for all jobs in the
      distribution (respecting dependencies).

      If an integer, Jenkins will initiate a build for all jobs
      without a successful build since the specified number of hours.

  orchestration.control.code: |
    ${orchestration.groovy.base}
    ${orchestration.groovy.scheduling}
    ${orchestration.groovy.reporting}

    // Dependency Data

    dependencies = ${jobs.dependencies/groovy}

    // Utilities

    time  = 0
    delta = 500

    running = []
    status = [:]

    def maybeScheduleBuild(name) {
      if (status[name][1]) {
        return
      }
      scheduleBuildUnlessDisabled(name)
    }

    def scheduleBuildUnlessDisabled(name) {
      job = Hudson.instance.getJob(name)
      if (job?.disabled) {
        return
      }

      scheduleBuild(name)
    }

    def scheduleBuild(name) {
      job = Hudson.instance.getJob(name)
      if (!job) {
        logMessage("  Scheduling build for \${name} failed: job not found!")
        return
      }

      logMessage("  Scheduling build for \${linkTo(job)}")
      run = job.scheduleBuild2(0, new Cause.UpstreamCause(build))
      if (run) {
        running << run
      }
    }

    def popBuild() {
      done = running.find { build -> build.isDone() }
      if (done && !done.isCancelled()) {
        running.remove(done)
        return done.get()
      }
    }

    def spin() {
      while (running) {
        build = popBuild()
        if (build) {
          logMessage("\${build.result} \${linkTo(build.project)}, \${linkTo(build)}, \${toSeconds(build.duration)} s")
          name = build.project.name
          if (!status[name]) { // For hooks
            status[name] = [ null, null ]
          }
          status[name][1] = build;
          startableDownstreams(name).each {
            project -> maybeScheduleBuild(project)
          }
          logMessage("\${running.size} build(s)")
        }

        Thread.sleep(delta)
        time += delta
      }
    }

    // Main part

    // Prepare build status map

    status["${prepare-hook-name|dummy-prepare-hook/}"] = [ null, null ]
    dependencies
      .each { entry -> status[entry.key] = [ null, null ] }

    ageLimitString = build.buildVariableResolver.resolve("ageLimit")
    if (ageLimitString && ageLimitString != "none") {
      ageLimit = use (groovy.time.TimeCategory) { (ageLimitString as int).hours.ago }
      markSuccessfulAndRecentBuilds(ageLimit)
    }

    // Run hooks and schedule builds for "root" projects

    @{run-prepare}

    logMessage("Running build jobs")

    try {
      dependencies.findAll { entry ->
        ((!entry.value || isStartable(entry))
         && !isSuccessfulBuildAvailable(entry.key))
      }
      .each { entry -> maybeScheduleBuild(entry.key) }
      spin()

      @{run-finish}
    } finally {
      // in case of error, cancel all running/scheduled jobs
      running.each { job -> job.cancel(true) }
    }

    // Report results

    reportResultsAndMaybeFailBuild()
