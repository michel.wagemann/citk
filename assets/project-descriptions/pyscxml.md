PySCXML (pronounced pixel) supplies an SCXML parser and interpreter for the Python programming language.
For an online console demonstrating the capabilities of PySCXML, check out http://pyscxml.spyderbrain.com
PySCXML doesn't respect backward compatibility, and won't until the SCXML standard goes from working draft to
recommended status. Recent changes may require some users to update documents written for previous versions of PySCXML.