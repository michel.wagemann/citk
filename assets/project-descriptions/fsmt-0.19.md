Often, high-level (functional) tests are carried out manually, implementing a tailored solution, e.g, via shell scripts
or launch files, for a specific system setup. Besides the effort of manual execution and supervision, current tests mostly
do not take timing and orchestration, i.e., required process start-up sequence, into account. Furthermore, successful execution
of components is not verified, which might lead to subsequent errors during the execution chain. Most importantly, all
this knowledge about the test and its environment is implicit, often hidden in the actual implementation of the tailored
test suite.

To overcome these issues, this framework introduces **a generic, event-driven and configurable state-machine based process** to automate:

   * a) Explicit Environment Setup, e.g., required Environment Variables
   * b) System Bootstrapping, e.g., Startup Sequence of Software Components
   * c) Functional System Tests
   * d) Automated Result Assessment based-on user Defined Metrics
   * e) Automated Exit and Clean Up Strategy

We have chosen a hierarchical state-based approach in order to inherit a well structured formalization, which enables us
to invoke the steps mentioned above, in the required order, and to explicitly model required test steps. Furthermore, the
state-chart model enables us to invoke states in parallel, or sequentially, which also makes orchestration, e.g.,
start-up of system components feasible and most importantly controllable. Lastly, errors during execution will prematurely
end the state-machine to prevent subsequent errors.

<h3>Thanks</h3>

This piece of software would not have been realized without the help of the following people:

Thank you **Ingo** and **Sebastian** for the many discussions about system- and software
engineering and especially system testing. Thank you **Norman** for your work as a student
assistant which really helped getting this done! Laslty, thank you **Sven** for promoting
and supervising this branch of my work.
