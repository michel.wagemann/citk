variables:
  'false': false
  'true': true
  git.local-branch: ${next-value|${tag|${branch|}}}
  git.wipe-out-workspace?: ${next-value|${false}}
  git.clean-before-checkout?: ${next-value|${true}}
  git.lfs-pull?: ${next-value|${false}}

  # fresh-copy|update|emulate-fresh-copy
  subversion.checkout-strategy: ${next-value|fresh-copy}

  mercurial.clean-before-checkout?: ${next-value|${true}}

  # This maps
  # scm = false => extra-requires:
  # scm = none  => extra-requires:none
  # scm = git   => extra-requires:git
  # etc.
  extra-requires.prefix:             [ "extra-requires:" ]
  extra-requires.variable-name:list: ${extra-requires.prefix}${scm|none}
  extra-requires.variable-name:      '@{extra-requires.variable-name:list}'

  extra-requires:
  - '@{${extra-requires.variable-name}|[]}'
  - '@{next-value|[]}'
  extra-requires:: # Note trailing colon in name
  - '@{next-value|[]}'
  extra-requires:none:
  - '@{next-value|[]}'
  extra-requires:archive:
  - program: wget
  - program: unp
  - program: unzip
  - program: tar
  - program: bzip2
  - program: gzip
  - '@{next-value|[]}'
  extra-requires:git:
  - program: git
  - program: git-lfs
  - '@{next-value|[]}'
  extra-requires:svn:
  - program: svn
  - '@{next-value|[]}'
  extra-requires:mercurial:
  - program: hg
  - '@{next-value|[]}'

aspects:
- name: archive.repository
  aspect: archive
  conditions:
    scm: archive
  variables:
    aspect.builder-constraints.archive:
    - [ before, <all> ]

    aspect.archive.url: ${repository}
    aspect.archive.filename: ${archive.filename|}
    aspect.archive.username: ${scm.username|}
    aspect.archive.password: ${scm.password|}

- name: git.repository
  aspect: git
  conditions:
    scm: git
  variables:
    aspect.builder-constraints.git:
    - [ before, <all> ]

    aspect.git.url: ${repository}
    aspect.git.branches:
    - ${commit|${tag|${branch}}}
    aspect.git.local-branch: ${git.local-branch}
    aspect.git.username: ${scm.username|}
    aspect.git.password: ${scm.password|}
    aspect.git.credentials: ${scm.credentials|}
    aspect.git.clone-timeout: ${git.clone-timeout|}
    aspect.git.wipe-out-workspace?: ${git.wipe-out-workspace?}
    aspect.git.clean-before-checkout?: ${git.clean-before-checkout?}
    aspect.git.checkout-submodules?: ${git.checkout-submodules?|}
    aspect.git.shallow?: ${git.shallow?|}
    aspect.git.lfs?: ${git.lfs-pull?|}

- name: subversion.repository
  aspect: subversion
  conditions:
    scm: svn
  variables:
    aspect.builder-constraints.subversion:
    - [ before, <all> ]

    aspect.subversion.revision: ${commit|}
    aspect.subversion.url: ${repository}/${branch-directory|/}/${sub-directory|/}
    aspect.subversion.credentials: ${scm.credentials|}
    aspect.subversion.local-dir: .
    aspect.subversion.checkout-strategy: ${subversion.checkout-strategy}

- name: mercurial.repository
  aspect: mercurial
  conditions:
    scm: mercurial
  variables:
    aspect.builder-constraints.mercurial:
    - [ before, <all> ]

    aspect.mercurial.url: ${repository}
    aspect.mercurial.branch: ${branch|}
    aspect.mercurial.tag: ${tag|}
    aspect.mercurial.credentials: ${scm.credentials|}
    aspect.mercurial.clean?: ${mercurial.clean-before-checkout?}
