# Problems:
#
# * "versions" section of distributions is formatted like
#
#     - - name
#       - version
#     - - name
#       - version
#
#   which looks stupid.
#
# * similarly for extra-{requires,provides}
#
# * versions and git commit could theoretically be parsed as numbers
#   -> may need !!str tag
#   OK, the dumper seems to take that into account
#
# * indentation of shell fragments may need hand-tuning:
#   + we do NOT want folding
#   + we probably DO want ident autodetection
#   + we MAY want some form of chomping
#
# * description-style multi-lines fields could benefit from folding
#   The dumper seems to do that already for single-line descriptions.

from collections import OrderedDict

import sys
import os

import json
import yaml
import yaml.nodes

# Load JSON

def fix_linebreaks(input_string):
    result = []
    outside, string, backslash = (None, None, None)

    def outside(c, result):
        result += c
        if c == '"':
            return string
        else:
            return outside

    def string(c, result):
        if c == '\n':
            result += '\\n'
            return string
        elif c == '\t':
            result += '\\t'
            return string
        elif c == '"':
            result += c
            return outside
        elif c == '\\':
            return backslash
        else:
            result += c
            return string

    def backslash(c, result):
        result += '\\'
        result += c
        return string

    outside, string, backslash = (outside, string, backslash)

    state = outside
    for c in input_string:
        state = state(c, result)

    return str(''.join(result))


def load_json_recipe(input):
    return json.loads(fix_linebreaks(open(input).read()),
                      object_pairs_hook=OrderedDict)


# Transform

def recursive_strip(thing):
    if isinstance(thing, dict):
        return type(thing)((recursive_strip(k), recursive_strip(v))
                           for k, v in thing.items())
    elif isinstance(thing, list):
        return [recursive_strip(v) for v in thing]
    elif isinstance(thing, str) or isinstance(thing, unicode):
        return thing.strip()
    else:
        return thing


def delete_name(thing):
    if 'name' in thing:
        del thing['name']
    return thing


def fix_extra_fields(thing):
    def rec(thing, path):
        if isinstance(thing, list) \
           and (path[-2:] == ['variables', 'extra-requires']
                or path[-2:] == ['variables', 'extra-provides']
                or path[-2:] == ['variables', 'extra-systems']
                or path[-2:] == ['variables', 'platform-provides']):
            def item(thing):
                if not isinstance(thing, list):
                    return thing
                new = None
                if len(thing) == 3:
                    new = {'nature': thing[0],
                           'target': thing[1],
                           'version': thing[2]}
                else:
                    new = {'nature': thing[0], 'target': thing[1]}
                return rec(new, path)
            return [item(e) for e in thing]
        # "versions": [
        #   [ "foo", "master" ]
        # ]
        elif isinstance(thing, list) \
             and len(path) == 2 \
             and path[0] == 'versions':
            name, versions = (thing[0], thing[1:])
            # [ "foo", "master" ]
            if len(versions) == 1 and isinstance(versions[0], unicode):
                return '%s%s@%s' % (name, ' '*(60 - (2 + len(name))), versions[0])
            # [ "foo", [ "master", { "param": 1 } ] ]
            if len(versions) == 1:
                return OrderedDict([('name', name),
                                    ('version', versions[0][0]),
                                    ('parameters', versions[0][1])])
            # [ "foo", "master", [ "a-branch", { "param": 1 } ] ]
            versions = [{'version':version}
                        for version in versions]
            return {'name': name, 'versions': versions}

        elif isinstance(thing, dict):
            return type(thing)((rec(k, path), rec(v, path + [k]))
                               for k, v in thing.items())
        elif isinstance(thing, list):
            return [rec(v, path + [i]) for (i, v)
                    in enumerate(thing)]
        else:
            return thing

    return rec(thing, [])


# Dump YAML

class MyDumper(yaml.SafeDumper):
    def expect_node(self, *args, **kwargs):
        # if isinstance(self.event, yaml.SequenceStartEvent) \
        #    and len(self.event.value) == 3 \
        #    and all([lambda x: isinstance(x, str) for x in self.event.value]):
        #     self.event.flow_style = True
        return super(yaml.SafeDumper, self).expect_node(*args, **kwargs)

    def choose_scalar_style(self):
        if isinstance(self.event.value, str) \
           or isinstance(self.event.value, unicode) \
           and '\n' in self.event.value:
            return '|'
        else:
            return super(yaml.SafeDumper, self).choose_scalar_style()

    def represent_ordereddict(dumper, data):
        value = []

        for item_key, item_value in data.items():
            node_key = dumper.represent_data(item_key)
            node_value = dumper.represent_data(item_value)

            value.append((node_key, node_value))

        return yaml.nodes.MappingNode(u'tag:yaml.org,2002:map', value)


MyDumper.add_representer(OrderedDict, MyDumper.represent_ordereddict)


def store_yaml_recipe(recipe, output):
    dumper = MyDumper(open(output, 'w'),
                      encoding='utf-8', allow_unicode=True,
                      default_flow_style=False)
    dumper.open()
    dumper.represent(recipe)
    dumper.close()


def convert_recipe(filename):
    try:
        if os.path.islink(filename):
            print('Not converting symlink %s' % filename)
        else:
            print('Converting %s' % filename)
            old = load_json_recipe(filename)
            new = fix_extra_fields(delete_name(recursive_strip(old)))
            store_yaml_recipe(new, filename)
    except Exception as e:
        print(filename)
        print(e)


def convert_recipes(recipes):
    if not recipes:
        recipes = list()
        for directory in ['projects', 'distributions',
                          'templates/citk',
                          'templates/toolkit',
                          'templates/ci',
                          'templates/ci-deploy',
                          'templates/_common']:
            for filename in os.listdir(directory):
                recipes.append(directory + '/' + filename)

    for recipe in recipes:
        convert_recipe(recipe)


# Driver

if __name__ == '__main__':
    convert_recipes(sys.argv[1:])
