catalog:
  title: Flobi Minimal
  version: nightly

variables:
  access: private

  platform-requires:
    trusty:
      packages:
      - libstdc++6-4.7-dev
      - libhighgui2.4
      - libcv2.4
      - libopencv-gpu2.4
      - libcvaux-dev
      - libcv-dev
      - libhighgui-dev
      - libopencv-gpu-dev
      - ros-indigo-ros-base
    ubuntu:
      packages:
      - blender
      - cmake
      - build-essential
      - python3
      - python3-dev
      - python3-setuptools
      - python3-yaml
      - g++-multilib
      - libc6-dev-i386
      - libgstreamer0.10-dev
      - libgstreamer-plugins-base0.10-dev
      - gstreamer0.10-alsa
      - gstreamer0.10-plugins-base
      - gstreamer0.10-plugins-good
      - gstreamer0.10-tools
      - gstreamer0.10-pulseaudio
      - libc6-dev-i386
      - libao-dev

  toolkit.dir: ${toolkit.volume}/${distribution-name}
  toolkit.volume: /tmp/

  filesystem.group/unix: clf

  prepare-hook-name: distribution-prepare-${distribution-name}
  prepare-hook/unix: |
    mkdir -p "${toolkit.dir}"
    chmod 2755 "${toolkit.dir}"

  finish-hook-name: distribution-finish-${distribution-name}
  finish-hook/unix: |
    find "${toolkit.dir}" -exec chgrp ${filesystem.group/unix} {} \;
    find "${toolkit.dir}" -type d -exec chmod 2755 {} \;
    find "${toolkit.dir}" -type f -exec chmod go+r {} \;
    find "${toolkit.dir}" -type f -perm /100 -exec chmod go+x {} \;
    chmod -R g+x "${toolkit.dir}"/bin

versions:
- vdemo                                                     @master
- libreflexxes                                              @1.2.6
- humotion                                                  @master
- xsc3                                                      @master
- flobi-description                                         @master
- flobi-startup-flobi-minimal                               @master
- flobi-sim                                                 @master
- hlrc_server                                               @master
- hlrc_client_cpp                                           @master
- hlrc_client_python                                        @master
- hlrc_tts_provider                                         @master
- marytts                                                   @v5.2
- marytts-voices                                            @5.1
- python3-rospkg                                            @1.0.37
- python3-catkin-pkg                                        @0.2.10
