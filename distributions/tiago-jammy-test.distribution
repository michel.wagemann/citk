variables:
  description: URDF from STEP
  recipe.maintainer:
    - Leroy Rügemer <lruegeme@techfak.uni-bielefeld.de>

  toolkit.volume:    /vol/tiago/tools
  toolkit.dir:       ${toolkit.volume}/urdf_from_step

  access: private
  ros.lang.disable: [geneus,genlisp]

  ros: one
  python.version: 3
  java.version: 17

  cmake.options:default:
    - CUDA_TOOLKIT_ROOT_DIR=${toolkit.dir}
    - CUDA_NVCC_FLAGS="--expt-relaxed-constexpr"
    - '@{next-value|[]}'

  shell.environment:default:
    - PYTHONPATH=${toolkit.dir}/lib/python3/dist-packages:\${PYTHONPATH}
    - ROS_MAVEN_REPOSITORY=file://${toolkit.dir}/share/repository/
    - ROS_MAVEN_DEPLOYMENT_REPOSITORY=${toolkit.dir}/share/repository/
    - '@{next-value|[]}'

  prepare-hook-name: distribution-prepare-${distribution-name}
  prepare-hook/unix: |
    mkdir -p ${toolkit.dir}
    for d in include lib/python3/dist-packages bin share; do mkdir -p "${toolkit.dir}/\$d"; done
    cd ${toolkit.dir}
    cd lib
    ln -s python3 python3.10
    cd python3
    ln -s dist-packages site-packages

  platform-requires:
    ubuntu:
      packages: 
        - rapidjson-dev
       
versions:

- swig@v4.1.1
- opencascade@V7_7_2
- pythonocc-core@7.7.2

#- urdf_from_step@main

- name: pip-install
  versions:
  - version: urdf_from_step
    parameters:
      pip.packages:
        - "svgwrite"
        - "numpy"
        - "matplotlib"
        - "PyQt5"
