This sub system of the STRANDS project is mostly based on ROS with currently ROS Hydro on Ubuntu precise
and ROS Indigo on Ubuntu trusty being officially supported by STRANDS (Indigo is the default since
January 2015). In this particular setup the virtual robot LINDA is supposed to navigate in a virtual
environment based on a topological map. All sensors, such as laser scanners, odometry data, robot
pose information, depth cameras, etc. are simulated using the strands-morse simulator (based on MORSE).

This system is used to simulate the robot's autonomous topological localisation and navigation capabilities
in, e.g, long-term (continuous) tasks without stressing the physical hardware. Waypoints can be interactively
set and checked whether the robot has reached its destination or not.