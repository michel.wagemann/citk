catalog:
  title: MILab
  version: nightly

variables:
  git.clean-before-checkout?: false
  subversion.checkout-strategy: update
  scm.trigger-disable?: ${next-value|${external?|false}}
  junit.allow-empty-results?: true
  email-notification.recipients:
  - rhaschke
  - gwalck
  email-notification.send-to-perpetrator?: true
  access: private
  ros: ${next-value|melodic}
  ros.install.prefix: ${toolkit.dir}/ros/${ros}
  c*flags: ${next-value|-march=core2 -O3 -Wno-deprecated-declarations}

  toolkit.volume: /vol/milab
  toolkit.dir: ${toolkit.volume}/bionic

  filesystem.group/unix: milab
  orchestration.success-results: [ SUCCESS, UNSTABLE ]
  prepare-hook-name: distribution-prepare-${distribution-name}
  prepare-hook/unix: |
    rm -rf "${toolkit.dir}"/*
    for d in include lib bin share; do mkdir -p "${toolkit.dir}/\$d"; done
    rosdep update

  finish-hook-name: distribution-finish-${distribution-name}
  finish-hook/unix: chmod -R a+rX,g-w,o-w "${toolkit.dir}"/*

  shell.environment:
  - '@{next-value|[]}'

versions:
- name: icl-nivision
  version: master
  parameters:
    external?: true
    cmake.options:
    - '@{next-value|[]}'
    - BUILD_WITH_RSB=FALSE
    - BUILD_WITH_RSC=FALSE
    - BUILD_WITH_RST=FALSE
- iclros_bridge                                             @master
- iclros_remote_recorder                                    @master
- tactile-filters                                           @master
- tactile-glove                                             @master
- ros-serial                                                @main
- tactile_bracelet                                          @master
- agni_serial_protocol                                      @master
- labjack-exodriver                                         @cmake_build
- gazebo-tactile-plugins                                    @wip-gazebo-9
- myrmex                                                    @master
- vicon_bridge                                              @ubi-master
- sr_common                                                 @ubi-kinetic-devel
- sr_utilities                                              @ubi-kinetic-devel
- sr_config                                                 @shadowrobot_130704_indigo
- sr_teleop                                                 @ubi-kinetic-devel
- cereal_port                                               @master
- mhsb_description                                          @master
- human_hand_description                                    @master
- myrmex_description                                        @kinetic-devel
- ros-omni_description                                      @master
- ros-phantom_omni                                          @master
- milab-library                                             @master
- milab-interface                                           @master
- milab-mss                                                 @master
- milab-audiolistener                                       @master
- milab-opencvvideolistener                                 @master
- milab-tactileglovelistener                                @master
- milab-tactilemodulelistener                               @master
- milab-genericlistener                                     @master
- milab-rosbagtriggerlistener                               @master
- milab-handtracker                                         @master-ros
- urdfdom_headers                                           @sensor-refactoring
- urdfdom                                                   @ros-sensor-refactoring
- urdf                                                      @sensor-parsing
- urdf_parser_py                                            @melodic-devel
- tactile-toolbox                                           @melodic-devel
- qtpropertybrowser                                         @master
- milab                                                     @master
- libssh2                                                   @master
- qssh2                                                     @master
