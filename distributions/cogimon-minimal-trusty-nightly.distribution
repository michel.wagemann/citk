catalog:
  title: CogIMon Minimal Simulation Distribution (Trusty)
  version: nightly
  description: opensource://cogimon-minimal.md
  image:
  - catalog://cosima.png
  - catalog://kuka-lwr-sim.png

variables:
  recipe.maintainer:
  - swrede@techfak.uni-bielefeld.de
  - dwigand@cor-lab.uni-bielefeld.de
  - flier@techfak.uni-bielefeld.de

  platform-requires:
    ubuntu:
      trusty:
        packages:
        - cmake
        - build-essential
        - libprotobuf-dev
        - protobuf-compiler
        - libboost-dev
        - python-setuptools
        - python-dev
        - libffi-dev
        - omniidl
        - omniorb
        - omniorb-idl
        - omniorb-nameserver
        - python-protobuf
        - libprotobuf-java
        - libboost-thread-dev
        - libboost-filesystem-dev
        - libboost-signals-dev
        - libboost-program-options-dev
        - libboost-system-dev
        - libboost-regex-dev
        - libeigen3-dev
        - libyaml-cpp-dev
        - ros-indigo-kdl-parser
        - liburdfdom-dev

  toolkit.dir: ${toolkit.volume}/${distribution-name}
  toolkit.volume: /tmp/cogimon-minimal-trusty-nightly/

  prepare-hook-name: distribution-prepare-${distribution-name}
  prepare-hook/unix: mkdir -p "${toolkit.dir}"

versions:
- spread                                                    @4.4
- libccd                                                    @master
- rsc                                                       @0.16
- rsb-protocol                                              @0.16
- rsb-cpp                                                   @0.16
- rsb-python                                                @0.16
- rst-proto                                                 @0.16
- rst-converters-python                                     @0.16
- rsb-tools-cl-binary                                       @0.16
- log4cpp                                                   @toolchain-2.8
- typelib                                                   @toolchain-2.8
- rtt                                                       @toolchain-2.8
- rtt_typelib                                               @toolchain-2.8
- ocl                                                       @toolchain-2.8
- orogen                                                    @toolchain-2.8
- cogimon-srdfdom                                           @indigo-devel
- xbotcoremodel                                             @master
- rtt-gazebo-clock-plugin                                   @master
- rtt-gazebo-embedded                                       @master
- rtt-gazebo-robot-sim                                      @master
- rtt-gazebo-lwr4plus-sim                                   @master
- rtt-core-extensions                                       @master
- cogimon-gazebo-models                                     @master
- cogimon-experimental                                      @master
- rst-rt                                                    @master
- rtt-rst-rt-typekit                                        @master
- rtt_dot_service                                           @master
- rtt-eigen-typekit                                         @indigo-devel
- rtt-kdl-typekit                                           @indigo-devel
- rtt-rsb-transport                                         @master
- cogimon-scenarios                                         @master
- fsmt                                                      @0.19
- pyscxml                                                   @v.0.8.5-fsmt
