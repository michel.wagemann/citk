minimum-generator-version: 0.25.22

templates:
- code-corlab
- common-lisp-executable
- base

catalog:
  title: Build Generator

variables:
  recipe.maintainer:
  - Jan Moringen <jmoringe@techfak.uni-bielefeld.de>

  description: >-
    A tool for generating Jenkins jobs from project and distribution
    descriptions.
  keywords:
  - continuous integration
  - deployment
  - jenkins
  - dsl
  - json

  redmine-project: build-generator

  repository: ${redmine.instance}/git/build-generator

  releases: [ '0.4', '0.5', '0.6', '0.7', '0.8', '0.9', '0.10',
              '0.11', '0.12', '0.13', '0.14', '0.15', '0.16', '0.17',
              '0.18', '0.19', '0.20', '0.21', '0.22', '0.23', '0.24',
              '0.25', '0.26', '0.27', '0.28', '0.29', '0.30', '0.31' ]
  release-branches: ${releases}
  branches:
  - master
  - '@{release-branches}'
  release-tags: release-${releases}
  tags: ${release-tags}
  numeric-version: '0.32'

  lisp.runtime-options: --dynamic-space-size 4Gb

  lisp.load-systems:
  - jenkins.project.commandline-interface
  lisp.entry-point: jenkins.project.commandline-interface:main

  describe-tag: |-
    'release-*'
  compute-version-command: |-
    tag=${describe-tag}
    git describe --long --dirty=-dirty --tags --match "\${tag}"                              \
      | sed -re 's/release-([0-9])+\.([0-9]+)-([0-9]+)-(.*)/echo "\\"\1.\$((\2 + 1)).\3\\""/' \
      | sh                                                                                   \

  shell.command: |
    # Get precise version from git and write into version-string.sexp
    ${compute-version-command}
      > version-string.sexp

    ${next-value}

  deployment.test.arguments: --version
  deployment.test: |
    BUILD_GENERATOR_CONFIG_FILES=%pwd ${toolkit.dir}/bin/${lisp.binary-name} ${deployment.test.arguments}

  extra-provides:
  - nature: program
    target: ${lisp.binary-name}
    version: ${numeric-version}

  extra-requires:
  - library: ssl
  - program: unp
  - program: unzip
  - program: file
  - program: git
  - '@{next-value|[]}'

versions:
- name: master
  variables:
    lisp.load-systems:
    - build-generator.commandline-interface
    lisp.entry-point: build-generator.commandline-interface:main

    deployment.test.arguments: version --changelog=1

# System and packages renamed jenkins[.project] -> build-generator in
# version 0.31
- pattern: '^(0\.(?:[4-9][0-9]|3[1-9]))$'
  variables:
    branch: ${match:0}

    numeric-version: ${match:0}

    describe-tag: |
      "\$(cat version-string.sexp                                                       \
         | sed -re 's/\"([0-9]+)\.([0-9]+)\.[0-9]+\"/echo \"release-\1.\$((\2 - 1))\"/' \
         | sh)"

    lisp.load-systems:
    - build-generator.commandline-interface
    lisp.entry-point: build-generator.commandline-interface:main

    deployment.test.arguments: version --changelog=1

# --changelog option of version command was added in version 0.22
- pattern: '^(0\.(?:30|2[2-9]))$'
  variables:
    branch: ${match:0}

    numeric-version: ${match:0}

    describe-tag: |
      "\$(cat version-string.sexp                                                       \
         | sed -re 's/\"([0-9]+)\.([0-9]+)\.[0-9]+\"/echo \"release-\1.\$((\2 - 1))\"/' \
         | sh)"

    deployment.test.arguments: version --changelog=1

# Experimental versions
- pattern: '(?:wip|feature)-.*'
  variables:
    branch: ${match:0}

    lisp.load-systems:
    - build-generator.commandline-interface
    lisp.entry-point: build-generator.commandline-interface:main

    deployment.test.arguments: version --changelog=1
