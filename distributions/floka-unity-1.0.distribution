catalog:
  title: Floka UNITY

variables:
  recipe.maintainer:
  - bcarlmey@techfak.uni-bielefeld.de
  - agross@techfak.uni-bielefeld.de
  access: private

  prepare-hook-name: distribution-prepare-${distribution-name}
  prepare-hook/unix: mkdir -p "${toolkit.dir}" ; chmod 2755 "${toolkit.dir}"

  finish-hook-name: distribution-finish-${distribution-name}
  finish-hook/unix: chmod -R g+x "${toolkit.dir}"/bin

  toolkit.volume: /vol/floka/unity-1.0
  toolkit.dir: ${next-value|${toolkit.volume}/${distribution-name}}

  ros: ${next-value|noetic}
  python.version: ${next-value|3}

  cuda-version: ${next-value|10.1}
  cuda.dir: /vol/cuda/${cuda-version}
  cuda.host.compiler: \$(which g++-8)
  cmake.options:default:
    - CUDA_TOOLKIT_ROOT_DIR=${cuda.dir}
    - CUDA_NVCC_FLAGS="--expt-relaxed-constexpr"
    - '@{next-value|[]}'
    
  unity-version: ${next-value|2020.3.24f1}
  unity.dir: /vol/unity/${unity-version}/bin
  

  message: |
    using cuda from ${cuda.dir}, set 'cuda.dir' to change. |
    using unity from ${unity.dir}, set 'unity.dir' to change.

versions:
- vdemo                                                     @master
- floka-startup-unity                                       @v1.0
- libreflexxes                                              @1.2.6
- humotion                                                  @master
- xsc3                                                      @master
- hlrc_server                                               @master
- hlrc_client_cpp                                           @master
- hlrc_client_python                                        @master
- hlrc_tts_provider                                         @master
- marytts                                                   @v5.2
- marytts-voices                                            @5.1
- flobi-description                                         @master
- floka-tcp-connector                                       @v1.0
- floka-unity-sim                                           @v1.0
