catalog:
  title: LSP CSRA STUDIES - Nao at Lab
  version: ${variant}

variables:
  access: private

  filesystem.group/unix: csra

  toolkit.dir: ${toolkit.volume}/releases/trusty/${distribution-name}
  toolkit.volume: /vol/csra/

  prepare-hook-name: distribution-prepare-${distribution-name}
  prepare-hook/unix: |
    mkdir -p "${toolkit.dir}"
    chmod 2755 "${toolkit.dir}"
    chgrp ${filesystem.group/unix} "${toolkit.dir}"
    chmod g+s "${toolkit.dir}"
    mkdir -p "${toolkit.dir}/var"
    chmod g+w "${toolkit.dir}/var"

  finish-hook-name: distribution-finish-${distribution-name}
  finish-hook/unix: |
    find "${toolkit.dir}" -exec chgrp ${filesystem.group/unix} {} \;
    find "${toolkit.dir}" -type d -exec chmod 2755 {} \;
    find "${toolkit.dir}" -type f -exec chmod go+r {} \;
    find "${toolkit.dir}" -type f -perm /100 -exec chmod go+x {} \;
    find "${toolkit.dir}/var" -type d -exec chmod 2775 {} \;
    find "${toolkit.dir}/var" -type f -exec chmod go+rw {} \;
    find "${toolkit.dir}/var" -type f -perm /100 -exec chmod go+x {} \;
    chmod -R g+x "${toolkit.dir}/bin"

  variant: studies-nao-at-lab
  java.jdk: Java8
  bco.registry.db.overwrite: 'TRUE'

versions:
- sbcl-binary                                               @1.3.0
- quicklisp                                                 @current
- cl-launch                                                 @master
- cl-architecture.service-provider                          @master
- cl-architecture.builder-protocol                          @master
- cl-network.spread                                         @master
- cl-protobuf                                               @master
- csra-modulefile                                           @trunk
- handle-controller-artguide                                @trunk
- spread                                                    @4.4
- spread-python                                             @1.5spread4
- rsc                                                       @0.12
- rsb-protocol                                              @0.12
- rsb-cpp                                                   @0.12
- rsb-java                                                  @wip-listener-pooling-0.12
- rsb-python                                                @0.12
- rsb-cl                                                    @master
- rsb-spread-cpp                                            @0.12
- rsb-tools-cpp                                             @0.12
- rsb-tools-cl-binary                                       @0.12
- rsb-tools-cl                                              @feature-norman-bridge
- rsbag-cl                                                  @feature-online-size-tracking
- rsbag-python                                              @0.12
- rsbag-tools-cl-binary                                     @0.12
- rsbag-tools-cl                                            @feature-recording-events
- rst-proto-csra-workaround                                 @0.12
- rst-proto-csra                                            @0.12
- rst-experimental-proto                                    @0.12
- rst-converters-cpp                                        @0.12
- rst-converters-python                                     @0.12
- rsb-gstreamer                                             @resume_stream_fix
- rsb-xml-cpp                                               @0.12
- rsb-xml-java                                              @0.12
- rsb-xml-python                                            @0.12
- rsb-opencv                                                @refactoring
- vdemo                                                     @0.5
- rct-tools                                                 @1.0.1
- rct-cpp                                                   @1.0.1
- rct-java                                                  @1.0.3
- rct-python                                                @1.0.3
- icl                                                       @9.8.0-with-opengl
- bayestracking                                             @features_bi
- nao-rpc                                                   @master
- xtt-python                                                @trunk
- nao-mss-artguide                                          @nao-at-app
- nao-idle                                                  @trunk
- nao-search-person                                         @trunk
- nao-woz                                                   @nao-at-app
- nao-mss-state                                             @trunk
- nao-directions                                            @master
- nao-faces-to-rsb                                          @trunk
- nao-gazecontroller                                        @master
- artguide-light-distractor                                 @trunk
- aldebaran-choregraphe                                     @2.1.4.13
- rsb-buffer                                                @0.12
- cmotion2d                                                 @trunk
- humavips-facedetection                                    @csra-0.1
- humavips-facetracker                                      @csra-0.1
- humavips-idrecognition                                    @learningver
- humavips-vfoa                                             @trunk
- humavips-headtracking                                     @trunk
- humavips-scenemanager                                     @rsb0.12
- pro-pm                                                    @master
- artguide-scenario-coordination-cfg                        @nao-at-app
- artguide-vdemo                                            @nao-at-app
- rsb-scxml-engine                                          @0.12
- lsp-csra-video-tools                                      @master
