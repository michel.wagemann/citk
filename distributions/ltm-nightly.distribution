catalog:
  title: Long Term Memory
  version: nightly

variables:
  access: private

  toolkit.dir: ${toolkit.volume}/releases/trusty/${distribution-name}
  toolkit.volume: /media/local_data/nkoester/vol/ltm

  prepare-hook-name: distribution-prepare-${distribution-name}
  prepare-hook/unix: |
    mkdir -p "${toolkit.dir}"
    chmod 2755 "${toolkit.dir}"
    chmod g+s "${toolkit.dir}"
    mkdir -p "${toolkit.dir}/var"
    chmod g+w "${toolkit.dir}/var"

  java.jdk: Java8
  bco.registry.db.overwrite: 'TRUE'

versions:
- spread                                                    @4.4
- spread-python                                             @1.5spread4
- ocvfacerec                                                @task-state-support
- rsc                                                       @0.13
- rsb-protocol                                              @0.13
- rsb-cpp                                                   @0.13
- rsb-java                                                  @0.13
- rsb-python                                                @0.13
- rsb-cl                                                    @0.13
- rsb-matlab                                                @0.13
- rsb-spread-cpp                                            @0.13
- rsb-tools-cpp                                             @0.13
- name: rsb-tools-cl
  versions:
  - version: wip-graphite-adapter
  - version: feature-norman-bridge
  - version: 0.13-rsb-only
- rsbag-cl                                                  @0.13
- rsbag-python                                              @0.13
- rsbag-tools-cl-binary                                     @0.13
- rsbag-tools-cl                                            @feature-recording-events
- sbcl-binary                                               @1.3.0
- quicklisp                                                 @current
- cl-launch                                                 @master
- cl-iterate-sequence                                       @master
- cl-architecture.service-provider                          @master
- cl-architecture.builder-protocol                          @master
- cl-network.spread                                         @master
- cl-protobuf                                               @master
- rst-proto-csra                                            @0.13
- rst-experimental-proto                                    @0.13
- rst-converters-cpp                                        @0.13
- rst-converters-python                                     @0.13
- rsb-lazy-converter-python                                 @0.13
- rsb-gstreamer                                             @resume_stream_fix
- rsb-xml-cpp                                               @0.13
- rsb-xml-java                                              @0.13
- rsb-xml-python                                            @0.13
- rsb-opencv                                                @refactoring
- rsb-video-writer                                          @master
- rsb-depth-sensors                                         @csra_adap
- rsb-person-tracking                                       @master
- rsb-process-monitor                                       @master
- rsb-host-monitor                                          @master
- vdemo                                                     @0.5
- ltm-system-startup                                        @master
- influxdb                                                  @master
- grafana                                                   @latest
- neo4j                                                     @2.2.10
- mongodb                                                   @ubuntu1404-3.2.7
- umongo                                                    @1.6.2
- ltm-core                                                  @master
- bco.registry.csra-db-deployer                             @master
- bco.registry.editor                                       @latest-stable
- jul                                                       @latest-stable
- jps                                                       @latest-stable
- bco.registry                                              @latest-stable
- 2d-map-visualisation                                      @master
- sendText                                                  @master
- receiveText                                               @master
- citk-scm-access-check                                     @master
- tf2_py                                                    @0.5.9
- rct-tools                                                 @1.0.1
- rct-cpp                                                   @1.0.3
- rct-java                                                  @1.0.5
- rct-python                                                @1.0.3
- ijenkinscli                                               @master
