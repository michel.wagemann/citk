catalog:
  title: LSP CSRA
  version: ${variant}

variables:
  variant: '0.6'
  datadir: ${toolkit.volume}/data/${variant}/
  ros: indigo
  rstexperimental.exit_on_ambiguity: '1'
  access: private

  toolkit.dir: ${toolkit.volume}/releases/trusty/${distribution-name}
  toolkit.volume: /vol/csra/

  filesystem.group/unix: csra

  prepare-hook-name: distribution-prepare-${distribution-name}
  prepare-hook/unix: |
    #!/bin/bash
    mkdir -p "${toolkit.dir}"/{bin,etc,opt,share,var}
    mkdir -p "${datadir}"
    ln -vfs -T "${datadir}" "${toolkit.dir}/share/data"
    chgrp ${filesystem.group/unix} "${toolkit.dir}"
    chgrp ${filesystem.group/unix} "${toolkit.dir}/var"
    chmod u=rwx,go=rx "${toolkit.dir}"
    chmod u=rwx,g=rwxs,o=rx "${toolkit.dir}/var"

  finish-hook-name: distribution-finish-${distribution-name}
  finish-hook/unix: |
    chgrp -R ${filesystem.group/unix} "${toolkit.dir}"
    chmod -R u=rwX,go=rX "${toolkit.dir}"
    chmod -R g+w "${toolkit.dir}/var"
    find "${toolkit.dir}/var" -type d -exec chmod g+s {} \;

  java.jdk: Java8
  bco.registry.db.overwrite: 'TRUE'
  bco.registry.db.home: share/bco/registry/db
  bco.registry.db.git.path: release-${variant}

versions:
- sbcl-binary                                               @1.3.0
- quicklisp                                                 @current
- cl-launch                                                 @hash-1698221
- cl-iterate-sequence                                       @hash-e5ee4c5
- cl-architecture.service-provider                          @hash-41b2bb0
- cl-architecture.builder-protocol                          @hash-06785b0
- cl-network.spread                                         @hash-2aacde1
- cl-protobuf                                               @hash-cfa3997
- spread                                                    @4.4
- spread-python                                             @1.5spread4
- pyscxml                                                   @v.0.8.5-fsmt
- fsmt                                                      @0.19
- libreflexxes                                              @1.2.6
- flobidev-core-humotion                                    @0.2
- flobidev-core-client_cpp                                  @0.2
- flobidev-core-control                                     @0.2
- flobidev-core-tools                                       @0.2
- flobidev-core-server                                      @0.2
- flobidev-core-sim                                         @0.2
- flobidev-basic-hlc_server_rsb                             @0.3
- flobidev-basic-hlc_client_java                            @0.3
- flobidev-basic-integration_tests                          @0.3
- vdemo                                                     @0.5
- rsc                                                       @0.15
- rsb-protocol                                              @0.15
- rsb-cpp                                                   @0.15
- rsb-java                                                  @0.15
- rsb-python                                                @0.15
- rsb-cl                                                    @0.15
- rsb-matlab                                                @0.15
- rsb-spread-cpp                                            @0.15
- rsb-tools-cpp                                             @0.15
- rsbag-python                                              @0.14
- rsbag-tools-cl-binary                                     @0.15
- rsbag-tools-cl                                            @feature-recording-events
- rst-proto                                                 @0.15
- rst-converters-cpp                                        @0.15
- rst-converters-python                                     @0.15
- rsb-gstreamer                                             @resume_stream_fix
- rsb-xml-cpp                                               @0.15
- rsb-xml-java                                              @0.15
- rsb-xml-python                                            @0.15
- rsb-buffer                                                @0.15
- rsb-opencv                                                @refactoring
- rsb-depth-sensors                                         @csra_release_0.5
- rsbag-cl                                                  @feature-online-size-tracking-0.13
- link-rsb-tools-cl                                         @rsb-toolscl0.13
- rsb-lazy-converter-python                                 @0.15
- rst-experimental-proto                                    @0.15
- name: rsb-tools-cl
  versions:
  - version: wip-graphite-adapter
  - version: feature-norman-bridge
  - version: 0.13-rsb-only
- rsb-person-tracking                                       @v0.5
- bayestracking                                             @features_bi
- pro-pm                                                    @v0.5
- openni2                                                   @OpenNI2_CSRA_0.5
- rsb-process-monitor                                       @hash-2.0
- rsb-host-monitor                                          @hash-2.0
- icl                                                       @9.8.0-with-opengl-opencv
- avin2-sensorkinect                                        @v0.93-5.1.2.1
- openhab-runtime                                           @v1.8.3.0
- openhab-addons                                            @v0.3.1
- openhab-cfg                                               @v0.3.0
- openhab-binding-rsb                                       @v1.2.1
- jul                                                       @v1.2.0
- jps                                                       @v3.1.2
- bco.dal                                                   @v1.2.0
- bco.registry.csra-db-deployer                             @release-0.6
- bco.registry                                              @v1.2.1
- bco.manager                                               @v1.2.1
- bco.registry.editor                                       @v1.2.0
- generic-display                                           @v0.5.0
- bco.bcozy                                                 @v0.2.3
- bco.eveson                                                @v1.0.0
- python3-rospkg                                            @1.0.20
- python3-catkin-pkg                                        @0.1.10
- morse-tarball                                             @1.2.1
- pamini                                                    @1.12
- dialogflow                                                @1.9
- marytts-timobaumann                                       @incrementalityChangesTake2
- marytts-voices                                            @5.1
- inprotk                                                   @csra_mary5
- inprotk-csra-conf                                         @1.6
- speech-command-proposer                                   @1.4.5
- speech-visualizer                                         @1.6.6
- sphinx4-recognizer                                        @1.1.2
- sphinx4-evaluation-tool                                   @1.1.1
- sphinx-rst-builder                                        @1.2
- jsgf-parser                                               @1.6.2
- verbmobil-model                                           @0.3
- cocolab-model                                             @0.5
- rsbperfmon-db-adapter                                     @1.0
- graphite-env                                              @stable
- graphite-web                                              @hash-csra-0.3
- carbon                                                    @hash-csra-0.3
- grafana                                                   @latest
- humavips-facedetection                                    @csra-0.1
- humavips-facetracker                                      @csra-0.1
- humavips-idrecognition                                    @learningver
- distributed_faceid_control                                @0.1.0
- dsl-ide-cor-lab-workbench                                 @49
- rsb-scxml-engine                                          @csra-0.15
- csra-scenario-coordination-cfg                            @13.2
- arbitration-service                                       @0.10
- ha-utils                                                  @0.5
- highlight-service                                         @0.5
- aa-skills                                                 @0.5
- location-light                                            @1.8
- task-proxy                                                @0.2
- ltm-core                                                  @v0.3
- neo4j                                                     @2.2.10
- mongodb                                                   @ubuntu1404-3.2.7
- ltm-java                                                  @0.1.0
- 2d-map-visualisation                                      @feature__fx
- object-detectionmock                                      @2.2
- lsp-csra-system-startup                                   @release-0.6
- lsp-csra-system-config                                    @release-0.6
- csra-modulefile                                           @0.3
- xqilla                                                    @2.3
- pquery                                                    @v1.4.0
- sitinf                                                    @b1.7
- caldavcalendar                                            @1.7
- hookah                                                    @1.4
- rsb-jack                                                  @csra-0.4
- adhoc                                                     @1.3
- csra-resources                                            @release-0.6
- libinkg                                                   @v0.2
- inkg-rsb-interface                                        @v0.3.0
- csra-utils                                                @release-0.6
- csra-robot-transforms                                     @v0.1.0
- citk-scm-access-check                                     @0.2
- floor-base                                                @v0.4.1
- floor-lib                                                 @v0.2.1
- tf2_py                                                    @0.5.9
- floor-view                                                @v0.4.0
- floor-input-emulator                                      @v0.3.0
- rct-tools                                                 @1.0.1
- rct-cpp                                                   @1.0.1
- rct-java                                                  @1.0.6
- rct-python                                                @1.0.3
- lsp-csra-docs                                             @release-0.6
- csra-personmanager                                        @release-0.1
- csra-screen-utils                                         @0.3
- ijenkinscli                                               @0.1
- dlib                                                      @v19.0
- gazetool                                                  @nogui-rsb
- pontoon                                                   @1.2
- ola                                                       @0.10.0
- csra-movinghead                                           @csra-0.5
- csra-soundscapes                                          @0.5
- ts-jenkins-connector                                      @0.2
- dataset-dlib                                              @dlib.net
- nmpt                                                      @hash-csra-0.3
- ximea-api                                                 @hash-csra-0.3
- simple-robot-gaze-tools                                   @hash-csra-0.3
- simple-robot-gaze-tools-audio                             @hash-csra-0.3
- hlrc_simple_robot_gaze                                    @rsb_support
- hlrc_simple_robot_gaze_plugins                            @hash-csra-0.3
- hlrc_client_python                                        @csra-stable
- opencv                                                    @2.4.10.1
- citk-version-updater                                      @v0.1.3
- csra-release-utils                                        @v0.1.3
- csra-proximity-tools                                      @v0.1.0
- rfid-device-manager-mockup                                @v1.2.1
