catalog:
  title: MILab
  version: xenial

variables:
  access: private

  ros: ${next-value|kinetic}

  git.clean-before-checkout?: false
  subversion.checkout-strategy: update
  junit.allow-empty-results?: true

  c*flags: ${next-value|-march=core2 -O3 -Wno-deprecated-declarations}

  toolkit.volume: /vol/milab
  toolkit.dir: ${toolkit.volume}/xenial

  filesystem.group/unix: milab

  prepare-hook-name: distribution-prepare-${distribution-name}
  prepare-hook/unix: |
    rm -rf "${toolkit.dir}"/*
    for d in include lib bin share; do mkdir -p "${toolkit.dir}/\$d"; done

  shell.environment:
  - '@{next-value|[]}'

versions:
- icl-milab                                                 @trunk
- tactile-filters                                           @master
- tactile-glove                                             @milab
- vicon_bridge                                              @ubi-master
- sr_common                                                 @ubi-kinetic-devel
- sr_utilities                                              @ubi-kinetic-devel
- sr_config                                                 @shadowrobot_130704_indigo
- sr_teleop                                                 @ubi-kinetic-devel
- cereal_port                                               @master
- mhsb_description                                          @master
- human_hand_description                                    @master
- myrmex_description                                        @kinetic-devel
- ros-omni_description                                      @master
- ros-phantom_omni                                          @master
- milab-library                                             @master
- milab-interface                                           @master
- milab-mss                                                 @master
- milab-audiolistener                                       @master
- milab-opencvvideolistener                                 @master
- milab-tactileglovelistener                                @master
- milab-tactilemodulelistener                               @master
- milab-genericlistener                                     @master
- milab-rosbagtriggerlistener                               @master
- milab-handtracker                                         @master-ros
- urdfdom_headers                                           @0.5
- urdfdom                                                   @0.5
- urdf                                                      @kinetic-sensor-parsing
- urdf_parser_py                                            @indigo-devel
- tactile-toolbox                                           @kinetic-devel
- qtpropertybrowser                                         @master
- milab                                                     @master
- libssh2                                                   @master
- qssh2                                                     @master
