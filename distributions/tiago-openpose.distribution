catalog:
  title: Tiago Software
  version: openpose

variables:
  description: Tiago Openpose stack
  recipe.maintainer:
  - Leroy Rügemer <lruegeme@techfak.uni-bielefeld.de>

  toolkit.volume:    /vol/tiago/${ros}
  toolkit.dir:       ${toolkit.volume}/openpose

  ros.underlay: /opt/pal/ferrum
  ros: melodic
  access: private

  java.jdk: Java11

  junit.allow-empty-results?: true

  shell.environment:${mode}:
  - PKG_CONFIG_PATH="${toolkit.dir}/lib/pkgconfig:\${PKG_CONFIG_PATH}"
  - CMAKE_PREFIX_PATH="${toolkit.dir}:\${CMAKE_PREFIX_PATH}"
  - ROS_MAVEN_REPOSITORY=file://${toolkit.dir}/share/repository/
  - ROS_MAVEN_DEPLOYMENT_REPOSITORY=${toolkit.dir}/share/repository/
  - CXXFLAGS="-march=core2"
#  - CXXFLAGS="-std=c++11 -march=core2"

  prepare-hook-name: distribution-prepare-${distribution-name}
  prepare-hook/unix: |
    mkdir -p ${toolkit.dir}
    cp -a ${base-deps.dir}/* ${toolkit.dir}/
    for d in include lib/python2.7/dist-packages bin share; do mkdir -p "${toolkit.dir}/\$d"; done
    unlink ${toolkit.dir}/lib64 || true
    ln -s ${toolkit.dir}/lib ${toolkit.dir}/lib64
    ln -s ${toolkit.dir}/lib/python2.7/dist-packages ${toolkit.dir}/lib/python2.7/site-packages

  cuda-version: ${next-value|10}
  cuda.host.compiler: \$(which g++-8)
  base-deps.dir: /vol/tiago/${ros}/cuda${cuda-version}

  platform-requires:
    ubuntu:
      packages: [g++-8]


versions:

# Peple
- name: cmu-openpose
  version: tiago-1.5.1
  parameters:
    cmake.options: 
      - BUILD_PYTHON=ON
      - '@{next-value|[]}'



# openpose-ros deps
- face_gender_age_detection_msgs@master
- ros-clf-perception-vision-msgs@master
- openpose_ros_msgs@tiago

- openpose_ros@tiago

- robocup_data@master

