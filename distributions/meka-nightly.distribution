catalog:
  title: Meka
  version: nightly

variables:
  access: private
  ros: kinetic

  rosetta-version: '0.3'

  finish-hook-name: distribution-finish-${distribution-name}
  finish-hook/unix: |
    find "${toolkit.dir}" -type d -exec chmod 2755 {} \;
    find "${toolkit.dir}" -type f -exec chmod go+r {} \;
    find "${toolkit.dir}" -type f -perm /100 -exec chmod go+x {} \;

  prepare-hook-name: distribution-prepare-${distribution-name}
  prepare-hook/unix: |
    mkdir -p "${toolkit.dir}"
    chmod 2755 "${toolkit.dir}"

  toolkit.volume: /vol/meka
  toolkit.dir: ${toolkit.volume}/nightly

  java.jdk: Java8
  rstexperimental.exit_on_ambiguity: '1'
  ldflags: ' '

  ros.lang.disable: [geneus,genlisp,genjava]


versions:
- name: synchronisation
  versions:
  - version: meka-marbel
  - version: meka-man
  - version: meka-matter
  - version: meka-gpu
  - version: meka-cloud
- xmltio                                                    @tar4275
- rtai                                                      @4.1.1
- linux-headers-rtai                                        @v3.14.17
- ethercat                                                  @1.5.2
- m3core                                                    @master
- m3meka                                                    @master
- m3ros_control                                             @master
- ros-m3meka_msgs                                           @master
- holomni-pcv                                               @master
- meka_omnibase_control                                     @master
- ros-m3_msgs                                               @master
- m3bie                                                     @master
- robocup-docs                                              @master
- robocup-system-startup                                    @projects
- robocup-system-config                                     @master
- robocup-robosync                                          @master

- vdemo                                                     @master
- marytts                                                   @v5.1.1
- marytts-voices                                            @5.1
- marytts-remote                                            @master
- ros-tobi_robot                                            @kinetic
- ros-navigation                                            @clf_merged
- ros-people_leg_detector                                   @clf-kinetic-cv3
- ros-people_tracking_filter                                @clf-kinetic-cv3
- ros-ira-laser-tools                                       @master
- pcl                                                       @pcl-1.9.1
- bayestracking                                             @tiago_upstream
- bayes_people_tracker                                      @master
- bart                                                      @ros_tf
- bart_ros                                                  @ros_tf
- xacro                                                     @kinetic-devel
- detector_msg_to_pose_array                                @1.2.0
- libreflexxes                                              @1.2.6
- python3-rospkg                                            @1.0.37
- python3-catkin-pkg                                        @0.2.10
- ros-meka_ros_publisher                                    @master
- ros-force_helper                                          @master
- ros-meka_stiffness_control                                @master
- ros-meka_description                                      @master
- ros-meka_bie_moveit_config                                @master
- ros-meka_bringup                                          @master
- ros-clf_posture                                           @master
- ros-meka_hand_shaker                                      @master
- ros-meka_hand_over                                        @master
- ros-meka_velocity_smoother                                @master
- dataset-dlib                                              @master
- ros-m3dashboard                                           @master
- ros-rqt_posture_recorder                                  @master
- ros-rqt_posture_editor                                    @master
- ros-meka_2dnav                                            @master
- dold_ros-dold_driver                                      @master
- dold_ros-dold_msgs                                        @master
- dold_meka_interface                                       @master
- ros-force_guiding                                         @master
- ros-keyboard_teleop                                       @llach
- ros-controls-control_msgs                                 @kinetic-devel
- opencv-plus-contrib-cuda                                 @3.3.1
- cuda                                                      @9.0
- cmu-openpose                                              @handtracking-stable
- cudnn                                                     @6.0
- caffe                                                     @openpose-stable
- face_gender_age_detection                                 @master
- hri-morse                                                 @master
- ros-tobi_sim                                              @master
- dart                                                      @v5.1.6
- sdfformat                                                 @sdf4
- zmq                                                       @v4.2.3
- cppzmq                                                    @v4.2.2
- tinyxml2                                                  @2.2.0
- name: ignition-math
  versions:
  - version: ign-math2
  - version: ign-math3
- ignition-msgs                                             @ign-msgs0
- gazebo                                                    @gazebo7_7.15.0_fixed
- gazebo-ros                                                @kinetic-devel
- gazebo-ros-pkgs-rosctrl                                   @kinetic-devel
- gazebo-ros-pkgs-plugins                                   @planar_move_changes
- gazebo-roboticsgroup-plugins                              @master
- gazetool                                                  @nogui-ros
- librealsense                                              @v2.19.1
- ros-realsense                                            @2.2.1
- rosbag-remote-record                                      @master

#######################################
## Flobi / Floka
#######################################

- humotion                                                  @master
- humotion_server_meka                                      @master
- flobi-description                                         @master
- xsc3                                                      @master
- hlrc_server                                               @master
- hlrc_client_cpp                                           @master
- hlrc_client_python                                        @master
- hlrc_tts_provider                                         @master
- flobi-sim                                                 @master
- ximea-api                                                 @master
- ximea-camera                                              @devel-indigo
- image-gpu-ros                                             @master
- gaze-relay                                                @master

#######################################
## FlexBE
#######################################

- flexbe_behavior_engine_clf                                @feature/flexbe_app
- flexbe_app                                                @feature/install_support
- meka_behaviors                                            @master

#######################################
## handover adaption
#######################################

- kdl                                                       @v1.3.1
- handover_adaption_jacobian_control                        @master
- handover_adaption_goal_uploader                           @master
- handover_adaption_traj_recorder                           @master

#######################################
## handover tracking and prediction
#######################################

- ros-robot_self_filter                                     @master
- ros-handover_prediction                                   @recoverystrat
- ros-handtracking                                          @recoverystrat
- ros-handtracking_msgs                                     @master

###############
## OpenFace
###############

- dlib                                                      @v19.14
- openface-cpp                                              @OpenFace_2.1.0
- ros-openface2_bridge                                      @meta_package



#######################################
## Msgs
#######################################
- ros-navigation_msgs@clf
- ros-hand_over_msgs@master
- ros-hand_shaker_msgs@master
- ros-force_guiding_msgs@master
- ros-tobi_world-msgs@master
- face_gender_age_detection_msgs@master
- ros-openni2-tracker-msgs@master
- ros-clf_posture_execution_msgs@master
- ros-clf-perception-vision-msgs@master
- pepper-clf-msgs@master
- hace_msgs@master
- bayes_people_tracker_msgs@tiago_upstream
