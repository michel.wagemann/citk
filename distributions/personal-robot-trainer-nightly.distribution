catalog:
  title: Personal Robot Trainer
  version: nightly

variables:
  access: private

  variant: nightly

  toolkit.dir: ${toolkit.volume}/releases/trusty/${distribution-name}
  toolkit.volume: /vol/scratch/sebschne/

  filesystem.group/unix: aistaff

  prepare-hook-name: distribution-prepare-${distribution-name}
  prepare-hook/unix: |
    mkdir -p "${toolkit.dir}"
    chmod 2755 "${toolkit.dir}"
    chgrp csra "${toolkit.dir}"
    chmod g+s "${toolkit.dir}"
    mkdir -p "${toolkit.dir}/var"
    chmod g+w "${toolkit.dir}/var"

  finish-hook-name: distribution-finish-${distribution-name}
  finish-hook/unix: |
    find "${toolkit.dir}" -exec chgrp ${filesystem.group/unix} {} \;
    find "${toolkit.dir}" -type d -exec chmod 2755 {} \;
    find "${toolkit.dir}" -type f -exec chmod go+r {} \;
    find "${toolkit.dir}" -type f -perm /100 -exec chmod go+x {} \;
    find "${toolkit.dir}/var" -type d -exec chmod 2775 {} \;
    find "${toolkit.dir}/var" -type f -exec chmod go+rw {} \;
    find "${toolkit.dir}/var" -type f -perm /100 -exec chmod go+x {} \;
    chmod -R g+x "${toolkit.dir}/bin"

versions:
- spread                                                    @4.4
- spread-python                                             @1.5spread4
- nemomath                                                  @trunk
- pyscxml                                                   @v.0.8.4-fsmt
- ocvfacerec                                                @task-state-support
- vdemo                                                     @0.5
- rsc                                                       @0.12
- rsb-protocol                                              @0.12
- rsb-cpp                                                   @0.12
- rsb-java                                                  @wip-listener-pooling-0.12
- rsb-python                                                @0.12
- rsb-matlab                                                @0.12
- rsb-tools-cpp                                             @0.12
- rsb-spread-cpp                                            @0.12
- rsb-bridge                                                @0.12
- rsb-tools-cl-binary                                       @0.12
- rsbag-tools-cl-binary                                     @0.12
- rsbag-python                                              @0.12
- rsb-vis-tools                                             @0.11
- rst-proto                                                 @0.12
- rst-converters-cpp                                        @0.12
- rst-converters-python                                     @0.12
- rsb-gstreamer                                             @resume_stream_fix
- rsb-xml-cpp                                               @0.12
- rsb-xml-java                                              @0.12
- rsb-xml-python                                            @0.12
- openni-nite-dev                                           @v1.4.2.4
- rsb-process-monitor                                       @master
- rsb-host-monitor                                          @master
- icewing                                                   @trunk
- avin2-sensorkinect                                        @v0.93-5.1.2.1
- icewing-kinectrecorder                                    @master
- icewing-contour-extraction                                @master
- icewing-rowing-kneedetector                               @master
- icewing-rowing-onlinesequencer                            @master
- icewing-rowing-sequencepoint                              @master
- icewing-userstate                                         @master
- icewing-intersect                                         @master
- icewing-depthbackground                                   @master
- icewing-stringpublisher                                   @master
- sensefloor_receiver                                       @master
- xtt-python                                                @trunk
- nao-mss                                                   @master
- nao-rpc                                                   @master
- nao-memory-adapter                                        @master
- nao-fitnessdevice-volumecontrol                           @master
- aldebaran-choregraphe                                     @2.1.4.13
- sports-decisions                                          @master
- sports-scenario                                           @master
- fitnessdevice                                             @master
- csra-sports-config                                        @master
- naobiofeedback                                            @master
- datastream_averager                                       @master
- prt-mongodb_adder                                         @master
- suunto_reader                                             @master
- attention-manager                                         @v0.1
- python3-csraconfigreader                                  @master
- rsbperfmon-db-adapter                                     @master
- rsb-scxml-engine                                          @0.12
- csra-scenario-coordination-cfg                            @8.2
- ltm-core                                                  @master
- lsp-csra-system-startup                                   @master
- csra-modulefile                                           @trunk
- xqilla                                                    @2.3
- libinkg                                                   @master
- inkg-rsb-interface                                        @master
- csra-scripts                                              @trunk
- citk-scm-access-check                                     @master
- link-appassembler-scripts                                 @trunk
- floor-base                                                @floor-base-0.2
- floor-lib                                                 @floor-lib-0.2
- floor-view                                                @floor-view-0.2
- ijenkinscli                                               @master
