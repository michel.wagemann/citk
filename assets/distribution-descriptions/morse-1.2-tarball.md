In this particular system version MORSE is utilized to simulate a simplistic robot following behavior.
In a virtual landscape environment, the CAT robot is intended to follow a MOUSE robot. Therefore, CAT
is equipped with two virtual semantic cameras. The semantic cameras report the location of the MOUSE
robot — if it's within their field of view. Based on the relative position of the "MOUSE" robot
(left of | right of), the CAT robot corrects its position towards the MOUSE. Initially, the MOUSE
starts moving, the CAT tries to follow its path and eventually tries to "catch it". The MOUSE robot,
as well as the CAT, are communicating via MORSE middleware (socket stream) with their controller scripts
that run outside of the simulation to allow for interactive communication and manipulation of the simulation.

Feel free to 1) replicate the system and 2) execute the linked experiment to verify our results.
Please see Linked Artifacts section. For a first glimpse, you may also check the linked video.