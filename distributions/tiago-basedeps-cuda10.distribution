catalog:
  title: Tiago Basedeps
  version: cuda10.1
  description: |
    Base Dependencies for Tiago Distributions
    Contains long running jobs
    This should be build once and then copied into the target distribution

variables:
  recipe.maintainer:
  - lruegeme@techfak.uni-bielefeld.de
  access: private

  prepare-hook-name: distribution-prepare-${distribution-name}
  prepare-hook/unix: mkdir -p "${toolkit.dir}" ; chmod 2755 "${toolkit.dir}"

  finish-hook-name: distribution-finish-${distribution-name}
  finish-hook/unix: |
    chmod -R g+x ${toolkit.dir}/bin

    # cleanup ros stuff
    cd ${toolkit.dir}
    rm _setup_util.py env.sh local_setup.bash local_setup.sh local_setup.zsh setup.bash setup.sh setup.zsh 
    rm -rf .catkin

  toolkit.volume: /vol/tiago/${ros}
  toolkit.dir: ${toolkit.volume}/cuda10

  cuda.host.compiler: \$(which g++-8)

  ros: ${next-value|melodic}

  platform-requires:
    ubuntu:
      packages: [g++-8]

versions:

- cmake@3.18.2

- cuda@10.1.105

- name: cudnn
  version: "7.6"
  parameters:
    cuda-version: '10.1'

- name: caffe
  version: cudnn8
  parameters:
    cmake.options:
    - '@{next-value|[]}'
    - CUDA_ARCH_NAME=Manual
    - CUDA_ARCH_BIN="60 61 62"
    - CUDA_ARCH_PTX=""
    - CUDA_HOST_COMPILER=${cuda.host.compiler|\$(which g++)}

- name: opencv-plus-contrib-cuda
  version: cuda-10
  parameters:
    timeout/minutes: 900

# Rebuild
- name: ros-vision_opencv # image_geometry cv_bridge
  version: ${ros}-clf